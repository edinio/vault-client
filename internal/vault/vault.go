package vault

import (
	"encoding/json"
	"fmt"
	"log"

	api "github.com/hashicorp/vault/api"
)

// API v2 specific path. Exclude /data for API v1.
const base = "/secret/data"

type KubeConfig struct {
	kubernetes_host                  string
	kubernetes_ca_cert               string
	token_reviewer_jwt               string
	disable_local_ca_jwt             bool
}

type KubeRoleConfig struct {
	name                             string
	bound_service_account_names      []string
	bound_service_account_namespaces []string
	policies                         []string
	token_ttl                        string
	token_max_ttl                    string
}

type Config struct {
	Token           string
	Address         string
	Path            string
	Debug           bool
	AuthType        string
	TokenType       string
	DefaultLeaseTTL string
	MaxLeaseTTL     string
	AuthConfig      KubeConfig
	RoleConfig      KubeRoleConfig
	// AuthConfig      struct{
	// 	kubernetes_host      string
	// 	kubernetes_ca_cert   string
	// 	token_reviewer_jwt   string
	// 	disable_local_ca_jwt bool	
	// }
	// RoleConfig      struct {
	// 	name                             string
	// 	bound_service_account_names      []string
	// 	bound_service_account_namespaces []string
	// 	policies                         []string
	// 	token_ttl                        string
	// 	token_max_ttl                    string
	// }
}

type Vault struct {
	client    *api.Client
	sysClient *api.Sys
	config    Config
}

func New(config Config) (Vault, error) {
	client, err := api.NewClient(&api.Config{Address: config.Address})
	if err != nil {
		return Vault{}, err
	}
	client.SetToken(config.Token)

	return Vault{client: client, sysClient: client.Sys(), config: config}, nil
}

// Enable Kubernetes Auth Method
func (v Vault) EnableKubernetesAuth(mountPath string) error {
	err := v.sysClient.EnableAuthWithOptions(
		mountPath,
		&api.EnableAuthOptions{
			Type: v.config.AuthType,
			Config: api.AuthConfigInput{
				DefaultLeaseTTL: v.config.DefaultLeaseTTL,
				MaxLeaseTTL:     v.config.MaxLeaseTTL,
				TokenType:       v.config.TokenType,
			},
		},
	)
	if err != nil {
		return fmt.Errorf("EnableAuth: %w", err)
	}
	if v.config.Debug {
		dat, err := json.Marshal(err)
		if err != nil {
			return fmt.Errorf("debug: %w", err)
		}
		log.Println(string(dat))
	}
	return nil
}

// Configure Kubernetes Auth Method
func (v Vault) AuthConfigKubernetes(mountPath string) (map[string]interface{}, error) {
	mountPath = fmt.Sprintf("/v1/sys/auth/%s", mountPath)
	r := v.client.NewRequest("POST", mountPath)
	body := map[string]interface{}{
		"kubernetes_host": v.config.AuthConfig.kubernetes_host,
		"kubernetes_ca_cert": v.config.AuthConfig.kubernetes_ca_cert,
		"token_reviewer_jwt": v.config.AuthConfig.token_reviewer_jwt,
		"disable_local_ca_jwt": v.config.AuthConfig.disable_local_ca_jwt,		
	}
	err := r.SetJSONBody(body)
	if err != nil {
		return map[string]interface{}{"request": r.Body}, fmt.Errorf("KubenetesAuthConfig: %w", err)
	}

	if v.config.Debug {
		dat, err := json.Marshal(err)
		if err != nil {
			return map[string]interface{}{"request": r.Body}, fmt.Errorf("debug: %w", err)
		}
		log.Println(string(dat))
	}
	return map[string]interface{}{"request": r.Body}, nil
}

// Configure Vault role for Kubernetes Auth Method
func (v Vault) AuthRoleConfigKubernetes(mountPath string) (map[string]interface{}, error) {
	mountPath = fmt.Sprintf(mountPath, v.config.RoleConfig.name)
	mountPath = fmt.Sprintf("/v1/sys/auth/%s", mountPath)
	r := v.client.NewRequest("POST", mountPath)
	body := map[string]interface{}{
		"name":  v.config.RoleConfig.name,
		"bound_service_account_names": v.config.RoleConfig.bound_service_account_names,
		"bound_service_account_namespaces": v.config.RoleConfig.bound_service_account_namespaces,
		"policies": v.config.RoleConfig.policies,
		"token_ttl": v.config.RoleConfig.token_ttl,
		"token_max_ttl": v.config.RoleConfig.token_max_ttl,
	}
	err := r.SetJSONBody(body)
	if err != nil {
		return map[string]interface{}{"request": r.Body}, fmt.Errorf("KubenetesRoleAuthConfig: %w", err)
	}

	if v.config.Debug {
		dat, err := json.Marshal(err)
		if err != nil {
			return map[string]interface{}{"request": r.Body}, fmt.Errorf("debug: %w", err)
		}
		log.Println(string(dat))
	}
	return map[string]interface{}{"request": r.Body}, nil
}

// Write upserts a new secret to a path.
func (v Vault) Write(key string, value map[string]interface{}) error {
	scr, err := v.client.Logical().Write(
		fmt.Sprintf("%s%s/%s", base, v.config.Path, key),
		map[string]interface{}{"data": value},
	)
	if err != nil {
		return fmt.Errorf("write: %w", err)
	}

	if v.config.Debug {
		dat, err := json.Marshal(scr)
		if err != nil {
			return fmt.Errorf("debug: %w", err)
		}
		log.Println(string(dat))
	}

	return nil
}

// Read retrieves the most recent version of an existing secret from the path.
func (v Vault) Read(key string) (map[string]interface{}, error) {
	scr, err := v.client.Logical().Read(fmt.Sprintf("%s%s/%s", base, v.config.Path, key))
	if err != nil {
		return nil, fmt.Errorf("read: %w", err)
	}
	if scr == nil {
		return nil, fmt.Errorf("path: not found")
	}

	if v.config.Debug {
		dat, err := json.Marshal(scr)
		if err != nil {
			return nil, fmt.Errorf("debug: %w", err)
		}
		log.Println(string(dat))
	}

	dat, ok := scr.Data["data"]
	if !ok {
		return nil, fmt.Errorf("secret: not found")
	}

	return dat.(map[string]interface{}), nil
}
