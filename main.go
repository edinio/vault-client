package main

import (
	"log"

	"gitlab.com/edinio/vault-client/internal/vault"
)

func main() {
	// authConfig := map[string]interface{}{
	// 	"kubernetes_host":      "https://127.0.0.1:8200",
	// 	"kubernetes_ca_cert":   "ca_crt",
	// 	"token_reviewer_jwt":   "jjknh",
	// 	"disable_local_ca_jwt": true,
	// } 
	// = struct {
	// 	name: "vault-csi",
	// 	bound_service_account_names:      "*",
	// 	bound_service_account_namespaces: "*",
	// 	policies:                         "default",
	// 	token_ttl:                        "999",
	// 	token_max_ttl:                    "9999",	
	// }
	// roleConfig := map[string]interface{}{
	// 	"name":                             "vault-csi",
	// 	"bound_service_account_names":      "*",
	// 	"bound_service_account_namespaces": "*",
	// 	"policies":                         "default",
	// 	"token_ttl":                        999,
	// 	"token_max_ttl":                    9999,
	// }

	client_config := &vault.Config{
		Token:           "hvs.WC28fopr5Z8WDglkimKcM60b",
		Address:         "http://127.0.0.1:8200",
		Path:            "/opensee", // /{application}/{environment}
		Debug:           true,
		AuthType:        "kubernetes",
		TokenType:       "service",
		DefaultLeaseTTL: "9999",
		MaxLeaseTTL:     "99999",
		AuthConfig:      vault.KubeConfig{
			kubernetes_host: "",
			kubernetes_ca_cert: "",
			token_reviewer_jwt: "",
			disable_local_ca_jwt: true,
		},
	}
	client_config.AuthConfig.kubernetes_host = "https://127.0.0.1:8200"

	client, err := vault.New(*client_config)
	// client, err := vault.New(vault.Config{
	// 	Token:           "hvs.WC28fopr5Z8WDglkimKcM60b",
	// 	Address:         "http://127.0.0.1:8200",
	// 	Path:            "/opensee", // /{application}/{environment}
	// 	Debug:           true,
	// 	AuthType:        "kubernetes",
	// 	TokenType:       "service",
	// 	DefaultLeaseTTL: "9999",
	// 	MaxLeaseTTL:     "99999",
	// })
	if err != nil {
		log.Fatal(err)
	}

	//Enbale kubernetes auth
	// if err := client.EnableKubernetesAuth("kube"); err != nil {
	// 	log.Fatal(err)
	// }

	//Configure kubernetes auth
	req, err := client.AuthConfigKubernetes("kube/config")
	if err != nil {
		log.Fatal(err)
	}
	log.Println("AuthConfig:", req)

	//Configure Vault role for kubernetes auth
	r, er := client.AuthRoleConfigKubernetes("kube/role/vault-csi")
	if er != nil {
		log.Fatal(er)
	}
	log.Println("AuthRoleConfig:", r)

	// EXAMPLE 1 (single)
	// Write
	// if err := client.Write("TOKEN", map[string]interface{}{"token": "XYZabc000"}); err != nil {
	// 	log.Fatal(err)
	// }
	// // Retrieve
	// token, err := client.Read("TOKEN")
	// if err != nil {
	// 	log.Fatal(err)
	// }
	// log.Println("TOKEN:", token)

	// EXAMPLE 2 (multiple)
	// Write
	// if err := client.Write("postgres", map[string]interface{}{"id": "inanzzz", "secret": "123123"}); err != nil {
	// 	log.Fatal(err)
	// }
	// Retrieve
	// secret, err := client.Read("postgres")
	// if err != nil {
	// 	log.Fatal(err)
	// }
	// log.Println("opensee:", secret)
}
